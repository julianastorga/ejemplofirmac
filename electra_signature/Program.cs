﻿using System;
using electra_signature.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace electra_signature
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            // Se crea un objeto de tipo Request. 

            // En BaseRequest.cs se encuentra definida la estructura del JSON
            // Dado que lo que varía es el Payload según el servicio a consumir, BaseRequest indica que Payload es un objeto de tipo PayloadRequestModel
            // Sin embargo, PayloadRequestModel es 'abstract' por lo que deberá ser implementado por un objeto que realmente represente el Payload y 
            // herede de PayloadRequestModel
            // En los modelos que describen los objetos JSON, mediante los attributos de Newtonsoft.JSON se indican el nombre que deberá tener el campo al serializarse
            // así como si es requerido o no. 

            BaseRequest Request = new BaseRequest()
            {
                Timestamp = (long)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds,
                Payload = new GetProductBalanceRequest()
                {
                    ProductId = "63689f3f-3f7f-4627-8859-6f659f334e54"
                },
                ExternalReference = "12"
            };

            // Se serializa el modelo utilizando la librería Newtonsoft.Json
            // Se le indica en la configuración del Serializador que se deben ignorar los valores nulos. 

            JsonSerializerSettings Settings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            String SerializedRequest = JsonConvert.SerializeObject(Request, Formatting.None, Settings);

            Console.WriteLine(SerializedRequest);

            // Se inicializa el Cliente que debe hacer los llamados.

            String Endpoint = "https://electra.serfintech.tk/api/c34bf3e0-5925-4051-8fa0-80fe80eeb925/execute/get_balance/on/082f2882-11f1-4360-800c-b1d731a6071c";

            HttpClient httpClient = new HttpClient();
            HttpRequestMessage APIRequest = BuildRequest(Endpoint, Request.Timestamp, SerializedRequest);
            HttpResponseMessage APIResponse = await httpClient.SendAsync(APIRequest);
            GetProductBalanceResponse ServiceResponse = await HandleResponse<GetProductBalanceResponse>(APIResponse);


            Console.WriteLine("Respuesta con estatus: " + ServiceResponse.Status);
            Console.WriteLine("\nLa transacción con el id: " + ServiceResponse.TransactionID + " indica que:");
            Console.WriteLine("\tEl producto: " + ServiceResponse.Payload.ProductID + "\n");
            Console.WriteLine("\tPosee un saldo de: " + ServiceResponse.Payload.MainBalance + "\n");
        }

        /*
         * Se crea un objeto de Request HTTP que lleva el URI. 
         * Esta función a su vez hace el llamado al generador de firmas para poder generar el string que deberá incluirse en el header 'x-signature'
         * Esta función además incluye el JSON ya serializado (como String) como contenido de tipo "application/json" en el Request HTTP
        */
        static HttpRequestMessage BuildRequest(String URI, long RequestTimestamp, String SerializedRequest)
        {
            String Signature = electra_signature.API.Crypto.Signature.Sign(RequestTimestamp, SerializedRequest);
            Console.WriteLine("Firma del request HTTP:\n");
            Console.WriteLine(Signature);

            Console.WriteLine("\nContenido del request HTTP:\n");
            Console.WriteLine(SerializedRequest);

            HttpRequestMessage HTTPRequest = new HttpRequestMessage(HttpMethod.Post, URI);
            HTTPRequest.Headers.Add("x-signature", Signature);
            HTTPRequest.Content = new StringContent(SerializedRequest, Encoding.UTF8, "application/json");

            Console.WriteLine("\nRequest:\n");
            Console.WriteLine(HTTPRequest.ToString());
            return HTTPRequest;
        }

        /*
         * Esta función maneja el Response HTTP y la debe convertir en un objeto de tipo BaseResponse
        */
        static async System.Threading.Tasks.Task<T> HandleResponse<T>(HttpResponseMessage APIResponse)
        {
            String APIResponseAsString = await APIResponse.Content.ReadAsStringAsync();
            Console.WriteLine("\nResponse:\n");
            Console.WriteLine(APIResponseAsString);
            return JsonConvert.DeserializeObject<T>(APIResponseAsString);
        }


    }
}
