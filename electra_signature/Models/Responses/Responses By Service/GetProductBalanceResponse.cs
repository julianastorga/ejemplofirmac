﻿using System;
using Newtonsoft.Json;
namespace electra_signature.Models
{

    public class GetProductBalanceResponse : BaseResponse
    {
        [JsonProperty(PropertyName = "payload", Required = Required.Default)]
        public new GetProductBalancePayload Payload { get; set; }
    }

    public class GetProductBalancePayload : PayloadResponseModel
    {
        [JsonProperty(PropertyName = "updated_at")]
        public String UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "inserted_at")]
        public String InsertedAt { get; set; }

        [JsonProperty(PropertyName = "id")]
        public String ID { get; set; }

        [JsonProperty(PropertyName = "overdraft")]
        public bool IsOverdraft { get; set; }

        [JsonProperty(PropertyName = "main_balance")]
        public Decimal MainBalance { get; set; }

        [JsonProperty(PropertyName = "product_id")]
        public String ProductID { get; set; }

    }
}
